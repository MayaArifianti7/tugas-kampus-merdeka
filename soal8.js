const dataPenjualanNovel = [
  {
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];
const getInfoPenjualan = (dataPenjualan) => {
  let result = {
      totalKeuntungan: "",
      totalModal: "",
      persentaseKeuntungan: "",
      produkBukuTerlaris: "",
      penulisTerlaris: ""
  }


  let tempArray = []
  let tempData = 0

  if (dataPenjualan == null) { 
      return "ERROR : Where is the parameters?"
  } else if (typeof dataPenjualan != "object") { 
      return "ERROR : INVALID data Type! It must be 'Object'"
  } else {
      for (let i = 0; i < dataPenjualan.length; i++) {
          tempArray[i] = (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual
          tempData += tempArray[i]
      }
      result.totalKeuntungan = tempData.toString()
      tempArray = []
      tempData = 0

      for (let i = 0; i < dataPenjualan.length; i++) {
          tempArray[i] = dataPenjualan[i].hargaBeli * (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok)
          tempData += tempArray[i]
      }
      result.totalModal = tempData.toString()
      tempArray = []
      tempData = 0

      for (let i = 0; i < dataPenjualan.length; i++) {
      tempArray[i] = dataPenjualan[i].totalTerjual
      }

      tempArray.sort((a, b) => a - b) 
      
      for (let i = 0; i < tempArray.length; i++) {
          if (dataPenjualan[i].totalTerjual == tempArray[tempArray.length - 1]) {
              result.penulisTerlaris = dataPenjualan[i].penulis
              result.produkBukuTerlaris = dataPenjualan[i].namaProduk
          }
      }
      tempArray = []
      tempData = 0

      tempData = (Number(result.totalKeuntungan) / Number(result.totalModal)) * (100)
      result.persentaseKeuntungan = tempData.toFixed(2) + "%"

      result.totalKeuntungan = "Rp. " + result.totalKeuntungan.split("").reverse().join("").match(/\d{1,3}/g).join(".").split("").reverse().join("")
      
      result.totalModal = "Rp. " + result.totalModal.split("").reverse().join("").match(/\d{1,3}/g).join(".").split("").reverse().join("")
  }

  return result
}

console.log(getInfoPenjualan(dataPenjualanNovel))



